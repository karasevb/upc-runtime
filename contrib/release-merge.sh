#!/bin/bash
# Script to automate merging and tagging at the end of a release branch

set -e
#set -x

## All the real work
function merge {
  local REPO=$1
  local BR=$2
  local DEV=$3

  ## Clone the repo
  local BRANCH="release/${BR}"
  git clone -b ${BRANCH} git@bitbucket.org:berkeleylab/${REPO}
  pushd ${REPO} >/dev/null

  ## Figure out versioning of the release
  local BUPC=$(echo ${BR} | rev | cut -d- -f1 | rev)
  local file keyword
  case ${REPO} in
    upc-translator) keyword=Version file=README ;;
    upc-runtime)    keyword=release file=ChangeLog ;;
    gasnet)         keyword=Release file=ChangeLog ;;
  esac
  REL=$(perl -ne "if (m/${keyword} (\d+\.\d+\.\d+)/) { print \$1; exit 0; }" -- ${file})
  if test -z "${REL}" ; then
    echo ERROR: Could not extract version info from ${REPO}
    exit 1
  fi

  ## Default DEV if not given
  if test -z "${DEV}" ; then
    if test $(echo ${BUPC} | cut -d. -f3) = '0'; then
      DEV=$(perl -e "@V = split(/\\./, '${REL}');" \
                 -e 'printf("%d.%d.1\n", $V[0], 1+$V[1]);')
    else
      OREV=$(git describe origin/develop | cut -d- -f2  | cut -d. -f-3)
      DEV=$(perl -e "@V = split(/\\./, '${OREV}');" \
                 -e 'printf("%d.%d.%d\n", $V[0], $V[1], 2+$V[2]);')
    fi
    if test -z "${DEV}" ; then
      echo ERROR: Could not determine next devel version
      exit 1
    fi
  fi

  ## Get user confirmation
  cat <<- EOF
	=====================================================================
	Preparing ${REPO} for berkeley-upc-${BUPC} release
	Merging branch ${BRANCH}
	   git-describe says $(git describe)
	  documentation says ${REL}
	New develop branch version should be ${DEV}
	=====================================================================
	EOF
  read -p "Enter 'Y' to continue.  " ans
  if test x$ans != xY; then
    echo Terminating
    exit 1;
  fi

  ## Attempt merge to 'master'
  git checkout --track -b master origin/master
  if git merge --no-stat --no-ff -m "Merge branch 'release/${REL}'" ${BRANCH}; then
    : # OK
  else
    echo "#########################################################################"
    echo "Failed to merge to master.  Dropping into shell.  'git commit' when done."
    echo "#########################################################################"
    set +e
    env PS1='FIX-MERGE$ ' bash -i
    set -e
  fi

  ## Attempt merge to 'develop' branch
  git checkout --track -b develop origin/develop
  if git merge --no-stat --no-ff -m "Merge branch 'release/${REL}' into develop" ${BRANCH}; then
    : # OK
  else
    echo "##########################################################################"
    echo "Failed to merge to develop.  Dropping into shell."
    echo "Expect a conflict on the version bump. Please keep HEAD version ($DEV)"
    echo "Run 'git commit' when done making changes."
    echo "##########################################################################"
    set +e
    env PS1='FIX-MERGE$ ' bash -i
    set -e
  fi

  ## Verify versioning on develop branch
  set +e
  case ${REPO} in
    upc-translator)
      grep -F -q "UPC translator version: release ${DEV}, built on" -- open64/osprey1.0/be/whirl2c/w2c_driver.cxx
      rc=$?
      ;;
    upc-runtime)
      grep -F -q "([UPCR_VERSION_LIT],[${DEV}])" -- acinclude.m4
      rc=$?
      ;;
    gasnet)
      grep -F -q "([GASNET_RELEASE_VERSION_MAJOR_D],[$(cut -d. -f1 <<<${DEV})])" -- configure.in &&
      grep -F -q "([GASNET_RELEASE_VERSION_MINOR_D],[$(cut -d. -f2 <<<${DEV})])" -- configure.in &&
      grep -F -q "([GASNET_RELEASE_VERSION_PATCH_D],[$(cut -d. -f3 <<<${DEV})])" -- configure.in
      rc=$?
      ;;
  esac
  set -e
  if test $rc != 0; then
    echo "#########################################################################"
    echo "Failed to locate version markings for '${DEV}' in develop branch."
    echo "Terminating."
    echo "#########################################################################"
    exit 1
  fi

  ## Misc merge fixup(s)
  case ${REPO} in
    upc-runtime)
      perl -pi -e "s/upcc-${BUPC}\.cgi/upcc-nightly.cgi/;" -- acinclude.m4 multiconf.conf.in
      echo "##########################################################################"
      echo "Please accept changes to restore upcc-nightly.cgi as default translator"
      echo "##########################################################################"
      git add -p
      git amend --no-edit
      ;;
  esac

  ## Tag master
  case ${REPO} in
    upc-translator)
      git tag -a berkeley-upc-${BUPC} -m "Berkeley UPC release ${BUPC}"              master
      ;;
    upc-runtime)
      git tag -a berkeley-upc-${BUPC} -m "Berkeley UPC release ${BUPC}"              master
      ;;
    gasnet)
      git tag    berkeley-upc-${BUPC}                                                master
      git tag -a gasnet-${REL}        -m "GASNet release ${REL}"                     master
      ;;
  esac

  ## Show how to push and what would be sent
  local PUSH="git push -n origin 'refs/heads/*' 'refs/tags/*' :${BRANCH}"
  echo =====================================================================
  echo $PUSH
  eval $PUSH
  echo =====================================================================

  ## Offer to push if user confirms
  local NONCE=${RANDOM}
  read -p "Enter '${NONCE}' to push.  " ans
  if test x$ans = x${NONCE}; then
    eval ${PUSH/-n/}
  else
    echo "${PUSH/-n/}" > push_cmd.${NONCE}
    echo DID NOT push - commands saved in ${REPO}/push_cmd.${NONCE}
  fi

  popd >/dev/null
}

#######################

if test $# -eq 1; then
  BR=$1
  merge gasnet         ${BR}
  merge upc-runtime    ${BR}
  merge upc-translator ${BR}
elif test $# -eq 2 || test $# -eq 3; then
  merge $*
else
  echo "usage1: $0 release-branch"
  echo "usage2: $0 REPO release-branch"
  echo "usage3: $0 REPO release-branch next-develop-version"
  exit 1
fi
exit 0
