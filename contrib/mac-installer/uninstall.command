#!/bin/sh

# parse arguments
for arg ; do
  case "$arg" in
      -t) echo RUNNING IN TEST-ONLY MODE
          DOIT="echo Would run:"
          ;;
      LOCALROOT=*)
          eval $arg
          echo "Using LOCALROOT: $LOCALROOT"
          ;;
      *)  echo "Unrecognized argument: $arg"
          echo "Usage: $0 [options]"
          echo "Supported options:"
          echo "  -t  "
          echo "      run in test-only mode - make no actual changes"
          echo "  LOCALROOT=/prefix/to/use  "
          echo "      perform non-privileged install in provided prefix"
          exit 1
          ;;
  esac
done

# get rooted
if test -z "$IN_BUPC_INSTALLER" ; then
  clear
  IN_BUPC_INSTALLER=1 ; export IN_BUPC_INSTALLER
  echo ""
  echo " ************** Berkeley UPC uninstaller **************"
  echo ""
  if test "`whoami`" != "root" -a "$LOCALROOT" = "" -a "$DOIT" = ""; then
    echo "Please enter your password to proceed:"
    /usr/bin/sudo "$0" "$@"
    result=$?
  else
    "$0" "$@"
    result=$?
  fi
  if test "$result" = "0" ; then
    echo "UNINSTALLER OPERATION SUCCESSFUL"
    exit 0
  else
    echo "UNINSTALLER OPERATION FAILED"
    exit 1
  fi
fi

mydir=`/usr/bin/dirname "$0"`
mydir=`cd "$mydir" && /bin/pwd`

packagename="berkeley_upc"
packageversion="2.18.0"

if test "$LOCALROOT" ; then
  installroot="$LOCALROOT"
else
  installroot="/usr/local"
fi
installdir="$installroot/$packagename-$packageversion"
installdir_generic="$installroot/$packagename"
globalbin="/usr/bin"
globalman="/usr/share/man"

$DOIT set -x -e

if test -z "$LOCALROOT" ; then
  # remove global links
  $DOIT rm -f "$globalbin/upcc"
  $DOIT rm -f "$globalbin/upcrun"
  $DOIT rm -f "$globalbin/upc_trace"
  $DOIT rm -f "$globalbin/upcdecl"
  $DOIT rm -f "$globalbin/gasnet_trace"

  $DOIT rm -f "$globalman/man1/upcc.1"
  $DOIT rm -f "$globalman/man1/upcrun.1"
  $DOIT rm -f "$globalman/man1/upc_trace.1"
  $DOIT rm -f "$globalman/man1/upcdecl.1"
fi

# remove actual install dir
$DOIT rm -f "$installdir_generic"
$DOIT rm -Rf "$installdir"

$DOIT set +x +e

echo "Uninstall complete."
exit 0
