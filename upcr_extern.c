/*
 * UPC runtime functions for external C/C++/MPI code
 *
 * Jason Duell <jcduell@lbl.gov>
 * $Source: bitbucket.org:berkeleylab/upc-runtime.git/upcr_extern.c $
 *
 */

#include <upcr_internal.h>

static void 
upcri_too_early(const char *func)
{
    fflush(stdout);
    /* NOTE: the test harness scans for "UPC Runtime error:" */
    fprintf(stderr, 
	    "UPC Runtime error: %s called before UPC runtime initialized\n", 
	    func);
    fflush(stderr);

    exit(-1);
}

/* Error if called too soon */
#if UPCR_DEBUG
  #define UPCRI_DIE_IF_TOO_EARLY(func)  \
   if (upcri_startup_lvl == upcri_startup_init)	\
       upcri_too_early(func);
#else
  #define UPCRI_DIE_IF_TOO_EARLY(func)  
#endif

int bupc_extern_mythread()
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_mythread");
    return (int) upcr_mythread();
}

int bupc_extern_threads()
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_threads");
    return (int) upcr_threads();
}

char * bupc_extern_getenv(const char *env_name)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_getenv");
    return upcr_getenv(env_name);
}

void bupc_extern_notify(int barrier_id)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_notify");
    upcr_notify(barrier_id, 0);
}

void bupc_extern_wait(int barrier_id)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_wait");
    upcr_wait(barrier_id, 0);
}

void bupc_extern_barrier(int barrier_id)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_barrier");
    upcr_notify(barrier_id, 0);
    upcr_wait(barrier_id, 0);
}

void * bupc_extern_alloc(size_t bytes)
{
    UPCR_BEGIN_FUNCTION();
    upcr_shared_ptr_t sptr;
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_alloc");
    sptr = upcr_alloc(bytes);
    if (upcr_isnull_shared(sptr))
	return NULL;
    return upcr_shared_to_local(sptr);
}

void * bupc_extern_all_alloc(size_t nblocks, size_t blocksz)
{
    UPCR_BEGIN_FUNCTION();
    upcr_shared_ptr_t sptr;
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_all_alloc");
    sptr = upcr_all_alloc(nblocks, blocksz);
    if (upcr_isnull_shared(sptr))
	return NULL;
    /* Performs equivalent of "(void *)sptr[MYTHREAD]", 
     * but w/o pointer arithmetic */
    return upcri_shared_remote_to_mylocal(sptr);
}


void bupc_extern_free(void *ptr, int thread)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_free");
    if (ptr == NULL)
	return;
    upcr_free(_bupc_local_to_shared(ptr, thread, 0));
}

void bupc_extern_all_free(void *ptr, int thread)
{
    UPCR_BEGIN_FUNCTION();
    UPCRI_DIE_IF_TOO_EARLY("bupc_extern_all_free");
    if (ptr == NULL)
	return;
    upcr_all_free(_bupc_local_to_shared(ptr, thread, 0));
}

void bupc_extern_config_info(const char **upcr_config_str, 
                             const char **gasnet_config_str,
                             const char **upcr_version_str,
                             int    *upcr_runtime_spec_major,
                             int    *upcr_runtime_spec_minor,
                             int    *upcr_debug,
                             int    *upcr_pthreads,
                             size_t *upcr_pagesize) {
  if (upcr_config_str)   *upcr_config_str = UPCR_CONFIG_STRING;
  if (gasnet_config_str) *gasnet_config_str = GASNET_CONFIG_STRING;
  if (upcr_version_str)  *upcr_version_str = UPCR_VERSION; 
  if (upcr_runtime_spec_major) *upcr_runtime_spec_major = UPCR_RUNTIME_SPEC_MAJOR;
  if (upcr_runtime_spec_minor) *upcr_runtime_spec_minor = UPCR_RUNTIME_SPEC_MINOR;
  #if UPCR_DEBUG
    if (upcr_debug) *upcr_debug = 1;
  #else
    if (upcr_debug) *upcr_debug = 0;
  #endif
  #if UPCRI_UPC_PTHREADS
    if (upcr_pthreads) *upcr_pthreads = 1;
  #else
    if (upcr_pthreads) *upcr_pthreads = 0;
  #endif
  if (upcr_pagesize) *upcr_pagesize = UPCR_PAGESIZE;
}

//-------------------------------------------------------------------------------
// support for bupc_tentative API
#include <bupc_tentative.h>

int bupc_tentative_version_major = BUPC_TENTATIVE_VERSION_MAJOR;
int bupc_tentative_version_minor = BUPC_TENTATIVE_VERSION_MINOR;

void (*bupc_tentative_init)(int *argc, char ***argv) = bupc_init;

void (*bupc_tentative_init_reentrant)(int *argc, char ***argv,
                         int (*pmain_func)(int, char **) ) = bupc_init_reentrant;

void (*bupc_tentative_exit)(int exitcode) = bupc_exit;

int (*bupc_tentative_mythread)(void) = bupc_extern_mythread;
int (*bupc_tentative_threads)(void) = bupc_extern_threads;

char * (*bupc_tentative_getenv)(const char *env_name) = bupc_extern_getenv;

void (*bupc_tentative_notify)(int barrier_id) = bupc_extern_notify;
void (*bupc_tentative_wait)(int barrier_id) = bupc_extern_wait;
void (*bupc_tentative_barrier)(int barrier_id) = bupc_extern_barrier;

void * (*bupc_tentative_alloc)(size_t bytes) = bupc_extern_alloc;
void * (*bupc_tentative_all_alloc)(size_t nblocks, size_t blocksz) = bupc_extern_all_alloc;
void (*bupc_tentative_free)(void *ptr, int thread) = bupc_extern_free;
void (*bupc_tentative_all_free)(void *ptr, int thread) = bupc_extern_all_free;

void (*bupc_tentative_config_info)(const char **upcr_config_str,
                                   const char **gasnet_config_str,
                                   const char **upcr_version_str,
                                   int    *upcr_runtime_spec_major,
                                   int    *upcr_runtime_spec_minor,
                                   int    *upcr_debug,
                                   int    *upcr_pthreads,
                                   size_t *upcr_pagesize) = bupc_extern_config_info;

extern void upcri_init_extern(void) {
  // this function is called by init to ensure upcr_extern.o is always present in the link,
  // which is a requirement for correct operation of the bupc_tentative_* API
  #define CHECK_TENT(sym) if (!(sym)) upcri_err("upcri_init_extern: missing " #sym)
  CHECK_TENT(bupc_tentative_version_major);
  CHECK_TENT(bupc_tentative_version_minor);
  CHECK_TENT(bupc_tentative_init);
  CHECK_TENT(bupc_tentative_init_reentrant);
  CHECK_TENT(bupc_tentative_exit);
  CHECK_TENT(bupc_tentative_mythread);
  CHECK_TENT(bupc_tentative_threads);
  CHECK_TENT(bupc_tentative_getenv);
  CHECK_TENT(bupc_tentative_notify);
  CHECK_TENT(bupc_tentative_wait);
  CHECK_TENT(bupc_tentative_barrier);
  CHECK_TENT(bupc_tentative_alloc);
  CHECK_TENT(bupc_tentative_all_alloc);
  CHECK_TENT(bupc_tentative_free);
  CHECK_TENT(bupc_tentative_all_free);
  CHECK_TENT(bupc_tentative_config_info);
}
